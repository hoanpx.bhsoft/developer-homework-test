import {
    GetProductsForIngredient,
    GetRecipes
} from "./supporting-files/data-access";
import {NutrientFact, Product, Recipe} from "./supporting-files/models";
import {
    GetCostPerBaseUnit,
    GetNutrientFactInBaseUnits
} from "./supporting-files/helpers";
import {RunTest, ExpectedRecipeSummary} from "./supporting-files/testing";

console.clear();
console.log("Expected Result Is:", ExpectedRecipeSummary);

const recipeData = GetRecipes(); // the list of 1 recipe you should calculate the information for
const recipeSummary: any = {}; // the final result to pass into the test function

/*
 * YOUR CODE GOES BELOW THIS, DO NOT MODIFY ABOVE
 * (You can add more imports if needed)
 * */

/**
 * The function helps to find the cheapest product per base unit.
 * @param {Product[]} products - List of product
 * @return {{product: Product, cost: number}} 
 *         - product: Cheapest product per base unit
 *         - cost: Price per base unit of the product
 */
const getCheapestCostProduct = (products: Product[]) => {
    // Should return undefined in case there are no products
    if (!products?.length) return;
    let cheapestCostProduct: Product | undefined;
    let cheapestCostProductCost: number | undefined;
    for (const product of products) {
        for (const supplierProduct of product.supplierProducts) {
            const currentCostPerBaseUnit = GetCostPerBaseUnit(supplierProduct);
            if (!cheapestCostProductCost || currentCostPerBaseUnit < cheapestCostProductCost) {
                cheapestCostProductCost = currentCostPerBaseUnit;
                cheapestCostProduct = product;
            }
        }
    }
    // Should return undefined in case all supplierProducts are empty
    if (!cheapestCostProduct) return;

    // This step may not be necessary in practice. 
    // I need to add this because the RunTest function is using JSON.stringify to compare two Objects
    // it will return "false" if the two objects are same but the key order is not same.
    cheapestCostProduct.nutrientFacts.sort((a, b) =>
        a.nutrientName.localeCompare(b.nutrientName)
    );
    return {
        product: cheapestCostProduct,
        cost: cheapestCostProductCost
    }
};

/**
 * This function is used to get the sumary of a recipe element
 * @param {Recipe} recipe - Recipe to get summary
 * @return {{cheapestCost: number, nutrientsAtCheapestCost: Record<string, NutrientFact}} 
 *         - cheapestCost: cheapest cost that recipe can be made for
 *         - nutrientsAtCheapestCost: summarise its nutritional information
 */
const getRecipeItemSummary = (recipe: Recipe) => {
    const nutrientsAtCheapestCost: Record<string, NutrientFact> = {}; // Use an object to store nutrient facts efficiently
    let cheapestCost = 0;
    for (const item of recipe.lineItems) {
        const products = GetProductsForIngredient(item.ingredient);
        const cheapestCostProduct = getCheapestCostProduct(products);
        if (!cheapestCostProduct?.cost || !cheapestCostProduct?.product) continue;
        for (const nutrientFact of cheapestCostProduct.product.nutrientFacts) {
            const nutrientFactInBaseUnits = GetNutrientFactInBaseUnits(nutrientFact);
            if (nutrientsAtCheapestCost[nutrientFact.nutrientName]) {
                // If the nutrient already exists in the object, we will add the amount to the existing one.
                nutrientsAtCheapestCost[nutrientFact.nutrientName].quantityAmount.uomAmount += nutrientFactInBaseUnits.quantityAmount.uomAmount;
            } else {
                nutrientsAtCheapestCost[nutrientFact.nutrientName] = nutrientFactInBaseUnits;
            }
        }
        cheapestCost += item.unitOfMeasure.uomAmount * cheapestCostProduct.cost;
    }
    return {
        cheapestCost,
        nutrientsAtCheapestCost,
    }
    
}

for (const recipe of recipeData) {
    recipeSummary[recipe.recipeName] = getRecipeItemSummary(recipe);
}

/*
 * YOUR CODE ABOVE THIS, DO NOT MODIFY BELOW
 * */
RunTest(recipeSummary);
